// ----------------------------------------------------------------EXPRESS BEGIN
var express = require('express');

var app = express();

app.use(express.json());
// ------------------------------------------------------------------EXPRESS END
// ------------------------------------------------------------------MONGO BEGIN
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var AutorSchema = new Schema(
  {
    nome: {type: String, required: true, max: 100},
  }
);

// Virtual for author's full name
AutorSchema
.virtual('nome')
.get(function () {
  return this.nome;
});

// Virtual for autor's URL
AutorSchema
.virtual('url')
.get(function () {
  return '/catalogo/autor/' + this._id;
});

//Export model
module.exports = mongoose.model('Autor', AutorSchema);

var LivroSchema = new Schema(
  {
    titulo: {type: String, required: true},
    autor: {type: Schema.Types.ObjectId, ref: 'Autor', required: true}, // referencia o autor
    paginas: {type: String, required: true},
  }
);

// Virtual for book's URL
LivroSchema
.virtual('url')
.get(function () {
  return '/catalogo/livro/' + this._id;
});

//Export model
module.exports = mongoose.model('Livro', LivroSchema);

// ------------------------------------------------------------------MONGO BEGIN

// Mais exemplos
// https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/mongoose
